'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _typeof2 = require('babel-runtime/helpers/typeof');

var _typeof3 = _interopRequireDefault(_typeof2);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _lodash = require('lodash.get');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function extend(target) {
  var sources = [].slice.call(arguments, 1);
  sources.forEach(function (source) {
    for (var prop in source) {
      target[prop] = source[prop];
    }
  });
  return target;
}

exports.default = function (manifestPath, options) {
  var manifest = void 0;
  var isManifestLoaded = false;

  options = options || {
    devMode: false
  };

  function loadManifest() {
    try {
      var _ret = function () {
        var data = {};

        if (_fs2.default.statSync(manifestPath).isDirectory()) {
          var manifestFiles = _fs2.default.readdirSync(manifestPath);
          if (manifestFiles.length === 0) {
            console.error('there are no asset manifest', e);
          }
          manifestFiles.forEach(function (manifest) {
            extend(data, JSON.parse(_fs2.default.readFileSync(_path2.default.resolve(manifestPath, manifest), 'utf8')));
          });
        } else {
          data = JSON.parse(_fs2.default.readFileSync(manifestPath, 'utf8'));
        }

        isManifestLoaded = true;
        return {
          v: data
        };
      }();

      if ((typeof _ret === 'undefined' ? 'undefined' : (0, _typeof3.default)(_ret)) === "object") return _ret.v;
    } catch (e) {
      console.log('could not load asset manifest', e);
    }
  }

  function getAsset(path) {
    if (options.devMode || !isManifestLoaded) {
      manifest = loadManifest();
    }

    if (manifest) {
      return (0, _lodash2.default)(manifest, path);
    } else {
      return path;
    }
  }

  return function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(ctx, next) {
      return _regenerator2.default.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              ctx.state.webpackAsset = getAsset;
              _context.next = 3;
              return next();

            case 3:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, undefined);
    }));

    return function (_x, _x2) {
      return _ref.apply(this, arguments);
    };
  }();
};