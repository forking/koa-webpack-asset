import fs from 'fs'
import path from 'path'
import get from 'lodash.get'

function extend (target) {
  let sources = [].slice.call(arguments, 1)
  sources.forEach((source) => {
    for (let prop in source) {
      target[prop] = source[prop]
    }
  })
  return target
}

export default (manifestPath, options) => {
  let manifest
  let isManifestLoaded = false

  options = options || {
    devMode: false
  }

  function loadManifest () {
    try {
      let data = {}

      if (fs.statSync(manifestPath).isDirectory()) {
        let manifestFiles = fs.readdirSync(manifestPath)
        if (manifestFiles.length === 0) {
          console.error('there are no asset manifest', e)
        }
        manifestFiles.forEach(function (manifest) {
          extend(data, JSON.parse(fs.readFileSync(path.resolve(manifestPath, manifest), 'utf8')))
        })
      } else {
        data = JSON.parse(fs.readFileSync(manifestPath, 'utf8'))
      }

      isManifestLoaded = true
      return data
    } catch (e) {
      console.log('could not load asset manifest', e)
    }
  }

  function getAsset (path) {
    if (options.devMode || !isManifestLoaded) {
      manifest = loadManifest()
    }

    if (manifest) {
      return get(manifest, path)
    } else {
      return path
    }
  }

  return async (ctx, next) => {
    ctx.state.webpackAsset = getAsset
    await next()
  }
}
